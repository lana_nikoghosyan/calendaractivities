<x-dashboard.layouts.app>
    <div class="container-fluid">
        <div class="card">
            <div class="card-title">
                    <h3 class="card-label">{{ __('__dashboard.user.form.show.title') }}</h3>
            </div>
            <div class="card-body">
            @foreach($activities as $activity)
                <p><a href="{{route('dashboard.activities.edit', $activity->id)}}">{{$activity->title}}</a></p>
            @endforeach
            </div>
        </div>
    </div>
</x-dashboard.layouts.app>
