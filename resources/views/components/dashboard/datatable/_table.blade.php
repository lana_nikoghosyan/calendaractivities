<div class="table-responsive">
    <table class="table table-hover" id="{{$id ?? '__data__table'}}">
        <thead>
        <tr>
            {{ $slot }}
        </tr>
        </thead>
    </table>
</div>
