<x-dashboard.layouts.app>
    <div class="container-fluid">
        <div class="card mb-4">

            <x-dashboard.layouts.partials.card-header :createRoute="route('dashboard.activities.create')"/>

            <div class="card-body">
                <x-dashboard.datatable._filters_form>
                    <div class="col-md-4 form-group">
                        <x-dashboard.form._input name="id" type="number"/>
                    </div>
                    <div class="col-md-4 form-group">
                        <x-dashboard.form._input name="title"/>
                    </div>
                </x-dashboard.datatable._filters_form>

                <x-dashboard.datatable._table>
                   <th data-key="id">{{ __('__dashboard.label.id') }}</th>
                   <th data-key="title">{{ __('__dashboard.label.title') }}</th>
                   <th data-key="date">{{ __('__dashboard.label.date') }}</th>
                   <th class="text-center">{{ __('__dashboard.label.actions') }}</th>
                </x-dashboard.datatable._table>
            </div>
        </div>
    </div>

    <x-slot name="scripts">
        <script src="{{ asset('/js/dashboard/activity/index.js') }}"></script>
    </x-slot>
</x-dashboard.layouts.app>




