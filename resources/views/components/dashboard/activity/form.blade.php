<x-dashboard.layouts.app>
    <div class="container-fluid">
        <div class="card mb-4">
            <x-dashboard.form._form
                :action="$viewMode === 'add' ? route('dashboard.activities.store') : route('dashboard.activities.update', $activity->id)"
                :method="$viewMode === 'add' ? 'post' : 'put'"
                :indexUrl="route('dashboard.activities.index')"
                :viewMode="$viewMode"
            >
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group required">
                            <x-dashboard.form._input name="title" :value="$activity->title"/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group required">
                            <x-dashboard.form._date name="date" class="datepicker" :value="$activity->date"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group required">
                            <x-dashboard.form._textarea name="description" :value="$activity->description" rows="3"/>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <x-dashboard.form._select title="user" allowClear :data="$users" multiple
                                                      dataName="user" class="select2"
                                                      defaultOption
                                                      :value="$userIds ?? []"
                                                      name="user_ids[]"/>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <div class="form-group required">
                            <x-dashboard.form.uploader._file
                                name="image"
                                :configKey="$activity->getFileConfigName()"
                                :value="$activity->image"
                            />
                        </div>
                    </div>
                </div>
            </x-dashboard.form._form>
        </div>
    </div>

    <x-slot name="scripts">
        <script src="{{ asset('/js/dashboard/activity/main.js') }}"></script>
    </x-slot>
</x-dashboard.layouts.app>


