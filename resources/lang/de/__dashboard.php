<?php

return array (
  'title' => 'Dashboard',
  'global' =>
  array (
    'log_out' => 'logout',
  ),
  'menu' =>
  array (
    'translation-manager' => 'Translation Manager',
    'users' => 'Users',
    'research-areas' => 'Research Areas',
  ),
  'user' =>
  array (
    'index' =>
    array (
      'title' => 'Users',
      'create' => 'Create User',
    ),
    'form' =>
    array (
      'add' =>
      array (
        'title' => 'Create User',
      ),
      'edit' =>
      array (
        'title' => 'Edit User',
        'description' => '',
      ),
    ),
  ),

  'profile' =>
  array (
    'index' =>
    array (
      'title' => 'My Profile',
      'description' => '',
    ),
    'dropdown' => 'My Profile',
  ),
  'button' =>
  array (
    'search' => 'Search',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'delete' => 'Delete',
    'add_more' => 'Add more',
    'crop' => 'Crop',
    'create' => 'Create',
    'reset' => 'Reset',
  ),
  'label' =>
  array (
    'id' => 'ID',
    'name' => 'Name',
    'slug' => 'Slug',
    'select' => 'Select',
    'publish_date' => 'Publish date',
    'created_at' => 'Created at',
    'actions' => 'Actions',
    'photo' => 'Photo',
    'title' => 'Title',
    'short_description' => 'Short description',
    'description' => 'Description',
    'meta_description' => 'Meta description',
    'meta_title' => 'Meta Title',
    'meta_keywords' => 'Meta Keywords',
    'uploaded_files' => 'Uploaded files',
    'uploaded_file' => 'Uploaded file',
    'show_status' => 'Show status',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'E-mail',
    'roles' => 'Roles',
    'role_ids' => 'Roles',
    'signature' => 'Signature',
    'password' => 'Password',
    'password_confirmation' => 'Confirm Password',
    'grouped_title' => 'Title',
    'grouped_link' => 'Link',
    'multiple_author' => 'Author',
    'release_date_time' => 'Release datetime',
    'avatar' => 'Avatar',
    'current_password' => 'Current password',
    'new_password' => 'New password',
    'new_password_confirmation' => 'Confirm New Password',
    'date' => 'Date',
    'image' => 'Image',
    'user' => 'User',
  ),
  'select' =>
  array (
    'option' =>
    array (
      'show_status_1' => 'Active',
      'show_status_2' => 'Inactive',
      'default' => 'Select',
      'all' => 'All',
    ),
  ),
  'modal' =>
  array (
    'are_you_sure' => 'Are you sure?',
    'confirm_action' => 'Confirm action',
    'title' =>
    array (
      'info' => 'Modal info title',
    ),
  ),
  'translations' =>
  array (
    'title' => 'Translations Manager',
  ),
  'message' =>
  array (
    'successfully_created' => 'Successfully Created',
    'successfully_updated' => 'Successfully Updated',
    'successfully_deleted' => 'Successfully Deleted',
  ),
  'tab' =>
  array (
    'general' => 'General',
  ),
  'activity' =>
  array (
    'form' =>
    array (
      'add' =>
      array (
        'title' => 'Create Activity',
      ),
      'edit' =>
      array (
        'title' => 'Edit Activity',
      ),
    ),
    'index' =>
    array (
      'title' => 'Activities',
    ),
  ),
);
