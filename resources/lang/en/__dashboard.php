<?php

return array (
  'message' =>
  array (
    'successfully_created' => 'Successfully Created',
    'successfully_updated' => 'Successfully Updated',
    'successfully_deleted' => 'Successfully Deleted',
  ),
  'label' =>
  array (
    'uploaded_files' => 'Uploaded files',
    'uploaded_file' => 'Uploaded file',
    'id' => 'ID',
    'title' => 'Title',
    'date' => 'Date',
    'actions' => 'Actions',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email' => 'E-mail',
    'roles' => 'Roles',
    'created_at' => 'Created at',
    'description' => 'Description',
    'publish_date' => 'Publish date',
    'show_status' => 'Show status',
    'name' => 'Name',
    'slug' => 'Slug',
    'select' => 'Select',
    'photo' => 'Photo',
    'short_description' => 'Short description',
    'meta_description' => 'Meta description',
    'meta_title' => 'Meta Title',
    'meta_keywords' => 'Meta Keywords',
    'role_ids' => 'Roles',
    'signature' => 'Signature',
    'password' => 'Password',
    'password_confirmation' => 'Confirm Password',
    'grouped_title' => 'Title',
    'grouped_link' => 'Link',
    'grouped_desc' => 'Desc',
    'multiple_author' => 'Author',
    'release_date_time' => 'Release datetime',
    'avatar' => 'Avatar',
    'current_password' => 'Current password',
    'new_password' => 'New password',
    'new_password_confirmation' => 'Confirm New Password',
    'image' => 'Image',
    'user' => 'User',
  ),
  'select' =>
  array (
    'option' =>
    array (
      'default' => 'Select',
      'all' => 'All',
      'show_status_1' => 'Active',
      'show_status_2' => 'Inactive',
    ),
  ),
  'button' =>
  array (
    'cancel' => 'Cancel',
    'add_more' => 'Add more',
    'delete' => 'Delete',
    'filters' => 'Filters',
    'reset' => 'Reset',
    'search' => 'Search',
    'save' => 'Save',
    'crop' => 'Crop',
    'create' => 'Create',
  ),
  'modal' =>
  array (
    'confirm_action' => 'Confirm action',
    'are_you_sure' => 'Are you sure?',
    'title' =>
    array (
      'info' => 'Modal info title',
    ),
  ),
  'tab' =>
  array (
    'general' => 'General',
  ),
  'profile' =>
  array (
    'dropdown' => 'My Profile',
    'index' =>
    array (
      'title' => 'My Profile',
      'description' => '',
    ),
  ),
  'global' =>
  array (
    'log_out' => 'logout',
  ),
  'title' => 'Dashboard',
  'menu' =>
  array (
    'users' => 'Users',
  ),
  'user' =>
  array (
    'index' =>
    array (
      'title' => 'Users',
      'create' => 'Create User',
    ),
    'form' =>
    array (
      'add' =>
      array (
        'title' => 'Create User',
      ),
      'edit' =>
      array (
        'title' => 'Edit User',
        'description' => '',
      ),
    ),
  ),

  'translations' =>
  array (
    'title' => 'Translations Manager',
  ),
  'datatable' =>
  array (
    'search_input_placeholder' => 'Search...',
  ),
  'activity' =>
  array (
    'form' =>
    array (
      'add' =>
      array (
        'title' => 'Create Activity',
      ),
      'edit' =>
      array (
        'title' => 'Edit Activity',
      ),
    ),
    'index' =>
    array (
      'title' => 'Activities',
    ),
  ),
);
