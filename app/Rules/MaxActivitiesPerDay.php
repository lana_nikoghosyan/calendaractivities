<?php

namespace App\Rules;

use App\Models\Activity\Activity;
use Illuminate\Contracts\Validation\Rule;

class MaxActivitiesPerDay implements Rule
{
    protected $activityId; // Store the current activity ID

    public function __construct($activityId = null)
    {
        $this->activityId = $activityId;
    }

    public function passes($attribute, $value)
    {
        // Check if there are already four activities for the same day
        $query = Activity::where('date', $value);

        if ($this->activityId !== null) {
            $query->where('id', '!=', $this->activityId);
        }

        $count = $query->count();

        return $count < Activity::MAX_ACTIVITIES_PER_DAY;
    }

    public function message()
    {
        return 'There should not be more than ' .  Activity::MAX_ACTIVITIES_PER_DAY . ' activities for the same day.';
    }
}
