<?php

namespace App\Services\API\User;

use App\Contracts\Role\IRoleRepository;
use App\Contracts\User\IUserRepository;
use App\Models\RoleAndPermission\Role;
use App\Services\BaseService;
use App\Services\File\FileTempService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class UserService extends BaseService
{
    /**
     * UserService constructor.
     *
     * @param IUserRepository $repository
     * @param IRoleRepository $roleRepository
     * @param FileTempService $fileService
     */
    public function __construct(
        IUserRepository           $repository,
        FileTempService           $fileService,
        protected IRoleRepository $roleRepository
    )
    {
        $this->repository = $repository;
        $this->fileService = $fileService;
    }

    /**
     * Function to create standard user
     *
     * @param array $data
     * @return Model
     */
    public function createStandardUser(array $data): Model
    {
        $data['password'] = Hash::make($data['password']);

        $user = $this->repository->create($data);
        $user->assignRole(Role::ROLE_USER);

        return $user;
    }

    public function getActivitiesByDateRange(array $data)
    {
        return auth()->user()->activities->whereHas();
    }
}
