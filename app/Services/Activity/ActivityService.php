<?php

namespace App\Services\Activity;

use App\Repositories\Activity\ActivityRepository;
use App\Repositories\User\UserRepository;
use App\Services\BaseService;
use Illuminate\Database\Eloquent\Model;

class ActivityService extends BaseService
{
    public function __construct(
        ActivityRepository                $repository,
        protected readonly UserRepository $userRepository)
    {
        $this->repository = $repository;
    }

    public function getViewData(int $id = null): array
    {
        $viewData = parent::getViewData($id);
        $viewData['users'] = $this->userRepository->getForSelect();

        if ($id) {
            $activity = $this->repository->find($id);
            $viewData['userIds'] = $activity->users->pluck('id')->all();
        }
        return $viewData;
    }

    public function createOrUpdate(array $data, int $id = null): Model
    {
        $activity = parent::createOrUpdate($data, $id);
        if (isset($data['user_ids'])) {
            $activity->users()->sync($data['user_ids']);
        } else {
            $users = $this->userRepository->all();

            $activity->users()->sync($users);
        }

        return $activity;
    }
}
