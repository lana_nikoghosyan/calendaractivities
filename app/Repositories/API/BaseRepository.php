<?php

namespace App\Repositories\API;

use App\Contracts\IBaseRepository;
use App\Models\Base\BaseModel;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use function App\Repositories\getRandomNumbers;

class BaseRepository implements IBaseRepository
{
    /**
     * @var Model
     */
    public Model $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @return Model
     */
    public function getInstance(): Model
    {
        return new $this->model();
    }

    /**
     * @param array $data
     * @return Model
     */
    public function create(array $data): Model
    {
        if ($this->model->defaultValues) {
            $data = [...$this->model->defaultValues, ...$data];
        }

        return $this->model->create($data);
    }

    /**
     * @param array $data
     * @return bool
     */
    public function insert(array $data): bool
    {
        return $this->model->insert($data);
    }

    /**
     * @param int|string $id
     * @param array $with
     * @param bool $throw
     * @param array $columns
     * @return Model|null
     */
    public function find(int|string $id, array $with = [], array $columns = [], bool $throw = true): ?Model
    {
        $builder = $this->model
            ->when(!empty($columns), function ($query) use ($columns) {
                $query->select($columns);
            })
            ->when(!empty($with), function ($query) use ($with) {
                $query->with($with);
            });

        return $throw ? $builder->findOrFail($id) : $builder->find($id);
    }

    /**
     * @param int|string $id
     * @param array $with
     * @param array $columns
     * @param bool $throw
     * @return Model|null
     */
    public function findMl(int|string $id, array $with = [], array $columns = [], bool $throw = true): ?Model
    {
        $builder = $this->model
            ->leftJoinMl()
            ->when(!empty($columns), function ($query) use ($columns) {
                $query->select($columns);
            })
            ->when(!empty($with), function ($query) use ($with) {
                $query->with($with);
            });

        return $throw ? $builder->findOrFail($id) : $builder->find($id);
    }

    /**
     * @param int $id
     * @param array $data
     * @return bool
     */
    public function findUpdate(int $id, array $data): bool
    {
        return $this->find($id)->update($data);
    }

    /**
     * @param int|string $id
     * @param array $where
     * @param array $with
     * @return Model
     */
    public function findOrFail(int|string $id, array $where = [], array $with = []): Model
    {
        return $this->model
            ->when(!empty($with), function ($query) use ($with) {
                $query->with($with);
            })
            ->when(!empty($where), function ($query) use ($where) {
                $query->where($where);
            })
            ->findOrFail($id);
    }

    /**
     * @param string $uuid
     * @return Model
     */
    public function firstOrFailByUUID(string $uuid): Model
    {
        return $this->model->where(['uuid' => $uuid])->firstOrFail();
    }

    /**
     * @param string $token
     * @return Model
     */
    public function firstOrFailByToken(string $token): Model
    {
        return $this->model->where(['token' => $token])->firstOrFail();
    }

    /**
     * @return Model|null
     */
    public function first(): ?Model
    {
        return $this->model->first();
    }

    /**
     * @param string $slug
     * @param array $with
     * @return Model|null
     */
    public function firstBySlug(string $slug, array $with = []): ?Model
    {
        $model = empty($with) ? $this->model : $this->model::with($with);

        return $model->whereSlug($slug)->first();
    }

    /**
     * @param string $slug
     * @param array $with
     * @param bool $throw
     * @return Model
     */
    public function firstOrFailBySlug(string $slug, array $with = [], bool $throw = true): Model
    {
        $model = empty($with) ? $this->model : $this->model::with($with);

        return $throw ? $model->whereSlug($slug)->firstOrFail() : $model->whereSlug($slug)->first();
    }

    /**
     * @return Collection
     */
    public function all(): Collection
    {
        return $this->model->all();
    }

    /**
     * @param array $with
     * @param array $columns
     * @return Collection
     */
    public function get(array $with = [], array $columns = []): Collection
    {
        $model = empty($with) ? $this->model : $this->model::with($with);

        return $columns ? $model->get($columns) : $model->get();
    }

    /**
     * @param array $with
     * @param array $columns
     * @return Collection
     */
    public function getWithMl(array $with = [], array $columns = []): Collection
    {
        $model = empty($with) ? $this->model : $this->model::with($with);

        return $columns ? $model->joinMl()->get($columns) : $model->joinMl()->get();
    }

    /**
     * @param array $whereIn
     * @param string $column
     * @param array $with
     * @param array|null $columns
     * @return Collection
     */
    public function getWhereIn(
        array $whereIn,
        string $column = 'id',
        array $with = [],
        ?array $columns = null
    ): Collection {
        $builder = $this->model
            ->when(!empty($with), function ($query) use ($with) {
                $query->with($with);
            })
            ->whereIn($column, $whereIn);

        return $columns ? $builder->get($columns) : $builder->get();
    }

    /**
     * @param array|null $columns
     * @param array $where
     * @param array $with
     * @return Collection
     */
    public function getWhere(array $where, array $columns = null, array $with = []): Collection
    {
        $builder = $this->model
            ->when(!empty($with), function ($query) use ($with) {
                $query->with($with);
            })
            ->where($where);

        return $columns ? $builder->get($columns) : $builder->get();
    }

    /**
     * @param array $where
     * @param array|null $columns
     * @param array $with
     * @return Collection
     */
    public function getWhereMl(array $where, array $columns = null, array $with = []): Collection
    {
        $builder = $this->model->joinMl()
            ->when(!empty($with), function ($query) use ($with) {
                $query->with($with);
            })
            ->where($where);

        return $columns ? $builder->get($columns) : $builder->get();
    }

    /**
     * @param array $where
     * @return int
     */
    public function getWhereCount(array $where): int
    {
        return $this->model->where($where)->count();
    }

    /**
     * @param array $where
     * @param array $with
     * @param array $columns
     * @return Model|null
     */
    public function firstWhere(array $where, array $with = [], array $columns = []): ?Model
    {
        return $this->model
            ->when(!empty($columns), function ($query) use ($columns) {
                $query->select($columns);
            })
            ->when(!empty($with), function ($query) use ($with) {
                $query->with($with);
            })
            ->where($where)
            ->first();
    }

    /**
     * @param array $where
     * @param array $with
     * @return Model
     */
    public function firstOrFailWhere(array $where, array $with = []): Model
    {
        return $this->model
            ->when(!empty($with), function ($query) use ($with) {
                $query->with($with);
            })
            ->where($where)
            ->firstOrFail();
    }

    /**
     * Function to get for select
     *
     * @param string $column
     * @param string $key
     * @return Collection
     */
    public function getForSelect(string $column = 'name', string $key = 'id'): Collection
    {
        return $this->model->pluck($column, $key);
    }

    /**
     * Function to get for select by join Ml
     *
     * @param string $column
     * @param string $key
     * @return Collection
     */
    public function getForSelectMl(string $column = 'name', string $key = 'id'): Collection
    {
        return $this->model->joinML()->pluck($column, $key);
    }

    /**
     * @param string $column
     * @param string $sortDir
     * @return Collection
     */
    public function getOrdered(string $column = 'sort_order', string $sortDir = 'desc'): Collection
    {
        return $this->model->orderBy($column, $sortDir)->get();
    }

    /**
     * @param array $where
     * @param int|null $count
     * @param array $with
     * @return Collection
     */
    public function getLatestWhere(array $where, int $count = null, array $with = []): Collection
    {
        return $this->model
            ->when(!empty($with), function ($q) use ($with) {
                $q->with($with);
            })
            ->where($where)->latest()
            ->take($count)->get();
    }

    /**
     * Pluck function
     *
     * @param string $key
     * @return Collection
     */
    public function pluck(string $key = 'id'): Collection
    {
        return $this->model->pluck($key);
    }

    /**
     * @param int $id
     * @param array $data
     * @return Model
     */
    public function update(int $id, array $data): Model
    {
        $model = $this->find($id);
        if ($model->defaultValues) {
            $data = [...$model->defaultValues, ...$data];
        }

        $model->update($data);
        return $model->refresh();
    }

    /**
     * @param array $whereData
     * @param array $data
     * @return Model
     */
    public function updateOrCreate(array $whereData, array $data): Model
    {
        return $this->model->updateOrCreate($whereData, $data);
    }

    public function firstOrCreate(array $whereData, array $data): Model
    {
        $model = $this->model->where($whereData)->first();

        if ($model) {
            return $model;
        }

        return $this->create($data);
    }

    /**
     * @param array $ids
     * @param array $data
     * @return bool
     */
    public function updateIn(array $ids, array $data): bool
    {
        return $this->model->whereIn('id', $ids)->update($data);
    }

    /**
     * @param array $whereData
     * @param array $data
     * @return bool
     */
    public function updateWhere(array $whereData, array $data): bool
    {
        return $this->model->where($whereData)->update($data);
    }

    /**
     * @param int|string $id
     * @return bool
     */
    public function destroy(int|string $id): bool
    {
        return $this->model->destroy($id);
    }

    /**
     * @param int $id
     * @return bool
     */
    public function softDelete(int $id): bool
    {
        $currentModel = $this->model->findOrFail($id);

        if (method_exists($currentModel, 'canDelete') && !$currentModel->canDelete()) {
            return false;
        }

        $updateData = [
            'show_status' => BaseModel::SHOW_STATUS_DELETED,
        ];

        if ($this->model->hasUserInfo) {
            $updateData = $updateData + ['deleted_user_id' => Auth::id(), 'deleted_user_ip' => request()->ip()];
        }

        return $this->model->where('id', $id)->update($updateData);
    }

    /**
     * @param Model $model
     * @param array $mlsData
     */
    public function saveMl(Model $model, array $mlsData): void
    {
        foreach ($mlsData as $lngCode => $mlData) {
            $model->mls()->updateOrCreate(
                [
                    $this->model->getForeignKey() => $model->id,
                    'lng_code' => $lngCode
                ],
                $mlData
            );
        }
    }

    /**
     * Function to make slug
     *
     * @param Model $model
     * @param string $name
     * @param int|null $id
     * @return string
     */
    public function makeSlug(Model $model, string $name, int $id = null): string
    {
        $slug = Str::slug($name);

        if ($id && $model->find($id)->slug == $slug) {
            return $slug;
        }

        $count = $model->whereRaw("slug REGEXP '^$slug(-[0-9]*)?$'")->count();

        $generatedSlug = $count > 0 ? "$slug-$count" : $slug;

        return $model->where('slug', $generatedSlug)->exists() ? $generatedSlug . getRandomNumbers() : $generatedSlug;
    }
}
