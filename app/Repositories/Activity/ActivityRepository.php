<?php

namespace App\Repositories\Activity;

use App\Repositories\BaseRepository;
use App\Models\Activity\Activity;

class ActivityRepository extends BaseRepository
{
    public function __construct(Activity $model)
    {
        parent::__construct($model);
    }

    public function getByDateRange(array $data)
    {
        return $this->model->whereHas('users', function ($q) {
            $q->where('user_id', auth()->id());
        })->when($data['range_start'], function ($q) use ($data) {
                $q->whereBetween('date', [$data['range_start'], $data['range_end']]);
            })->get();
    }
}
