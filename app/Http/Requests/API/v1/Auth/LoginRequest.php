<?php

namespace App\Http\Requests\API\v1\Auth;

use App\Http\Requests\API\BaseRequest;

class LoginRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'email' => 'required|email|string_with_max',
            'password' => 'required|min:8',
        ];
    }
}
