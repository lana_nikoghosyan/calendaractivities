<?php

namespace App\Http\Requests\API\v1\Auth;

use App\Http\Requests\API\BaseRequest;
use App\Models\User\User;
use Illuminate\Validation\Rules\Password;

class RegisterRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'first_name' => 'required|string_with_max',
            'last_name' => 'required|string_with_max',
            'email' => 'required|email|string_with_max|unique:' . User::getTableName(),
            'password' => [
                'required',
                'confirmed',
                Password::min(8)
                    ->mixedCase()
                    ->numbers()
            ],
            'i_agree' => 'required|in:1'
        ];
    }
}
