<?php

namespace App\Http\Requests\API\v1;

use App\Http\Requests\API\BaseRequest;
use App\Models\User\User;
use Illuminate\Validation\Rules\Password;

class ActivitiesDateRangeRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'range_start' => 'nullable|date|date_format:Y-m-d',
            'range_end' => 'nullable|required_with:range_start|date|date_format:Y-m-d|after:range_start',
        ];
    }
}
