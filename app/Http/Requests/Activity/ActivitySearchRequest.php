<?php

namespace App\Http\Requests\Activity;

use App\Http\Requests\Core\DatatableSearchRequest;

class ActivitySearchRequest extends DatatableSearchRequest
{
    public function rules(): array
    {
        return parent::rules() + [
                'f.id' => 'nullable|integer_with_max',
                'f.title' => 'nullable|string_with_max',
            ];
    }
}
