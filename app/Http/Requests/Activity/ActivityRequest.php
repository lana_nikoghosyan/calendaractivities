<?php

namespace App\Http\Requests\Activity;

use App\Models\User\User;
use App\Rules\MaxActivitiesPerDay;
use Illuminate\Foundation\Http\FormRequest;

class ActivityRequest extends FormRequest
{
    public function rules(): array
    {
        $activityId = $this->route('activity')?->id;

        return [
            'title' => 'required|string_with_max',
            'date' => ['required', 'after_or_equal_today', new MaxActivitiesPerDay($activityId)],
            'description' => 'required|text_with_max',
            'image' => 'required|string_with_max',
            'user_ids' => 'nullable|array',
            'user_ids.*' => 'nullable|exist_validator:' . User::getTableName()
        ];
    }
}
