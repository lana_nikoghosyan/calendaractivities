<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Requests\Activity\ActivityRequest;
use App\Http\Requests\Activity\ActivitySearchRequest;
use App\Models\Activity\ActivitySearch;
use App\Models\Activity\Activity;
use App\Repositories\Activity\ActivityRepository;
use App\Services\Activity\ActivityService;
use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\View\View;

class ActivityController extends BaseController
{
    public function __construct(
        ActivityService $service,
        ActivityRepository $repository
    ){
        $this->service = $service;
        $this->repository = $repository;
    }

    public function index(): View
    {
        return $this->dashboardView('activity.index');
    }

    public function getListData(ActivitySearchRequest $request): array
    {
        $searcher = new ActivitySearch($request->validated());

        return [
            'recordsTotal' => $searcher->totalCount(),
            'recordsFiltered' => $searcher->filteredCount(),
            'data' => $searcher->search(),
        ];
    }

    public function create(): View
    {
        return $this->dashboardView(
            view: 'activity.form',
            vars: $this->service->getViewData()
        );
    }

    public function store(ActivityRequest $request): JsonResponse
    {
      $this->service->createOrUpdate($request->validated());

        return $this->sendOkCreated([
            'redirectUrl' => route('dashboard.activities.index')
        ]);
    }

    public function edit(Activity $activity): View
    {
         return $this->dashboardView(
            view: 'activity.form',
            vars: $this->service->getViewData($activity->id),
            viewMode: 'edit'
        );
    }

    public function update(ActivityRequest $request, Activity $activity): JsonResponse
    {
      $this->service->createOrUpdate($request->validated(), $activity->id);

        return $this->sendOkUpdated([
            'redirectUrl' => route('dashboard.activities.index')
        ]);
    }

    public function destroy(Activity $activity): JsonResponse
    {
      $this->service->delete($activity->id);

        return $this->sendOkDeleted();
    }
}
