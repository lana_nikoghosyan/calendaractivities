<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Traits\Helpers\ApiResponses;

class BaseController extends Controller
{
    use ApiResponses;

    /**
     * @var null
     */
    protected $service = null;

    /**
     * @var null
     */
    protected $repository = null;
}
