<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\v1\Auth\LoginRequest;
use App\Http\Requests\API\v1\Auth\RegisterRequest;
use App\Http\Resources\v1\User\UserResource;
use App\Models\User\User;
use App\Services\API\User\UserService;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class AuthController extends BaseController
{
    public function __construct(
        protected UserService $userService
    )
    {
    }

    public function register(RegisterRequest $request): JsonResponse
    {
        $user = $this->userService->createStandardUser(data: $request->validated());

        $response = self::responseDataWithCookie($user);

        return $this->createdResponse($response['response'])->withCookie($response['cookie']);
    }

    public function login(LoginRequest $request): JsonResponse
    {
        if (!Auth::attempt($request->only('email', 'password'))) {
            return $this->unprocessableResponse(['credentials' => [__('auth.failed')]]);
        }

        $response = self::responseDataWithCookie(Auth::user());

        return $this->okResponse($response['response'])->withCookie($response['cookie']);
    }

    public function refresh(): JsonResponse
    {
        $response = self::responseDataWithCookie(Auth::user());

        return $this->okResponse($response['response'])->withCookie($response['cookie']);
    }

    public function logout(): JsonResponse
    {
        auth()->user()->currentAccessToken()->delete();

        return $this->noContentResponse();
    }

    public function me(): JsonResponse
    {
        return $this->okResponse(data: new UserResource(\auth()->user()));
    }

    private function responseDataWithCookie(User $user): array
    {
        $token = $user->createToken('token')->plainTextToken;

        $cookie = cookie('jwt', $token, config('sanctum.expiration'));

        return [
            'response' => [
                'token' => $token,
                'user' => new UserResource($user),
            ],
            'cookie' => $cookie
        ];
    }
}
