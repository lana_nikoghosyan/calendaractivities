<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\API\BaseController;
use App\Http\Requests\API\v1\ActivitiesDateRangeRequest;
use App\Repositories\Activity\ActivityRepository;
use App\Services\API\User\UserService;

class ActivityController extends BaseController
{
    public function __construct(
        ActivityRepository $repository
    ) {
        $this->repository = $repository;
    }
    public function getByDateRange(ActivitiesDateRangeRequest $request)
    {
        $activities = $this->repository->getByDateRange($request->validated());

        return $this->okResponse(\App\Http\Resources\v1\Activity\ActivityResource::collection($activities));
    }
}
