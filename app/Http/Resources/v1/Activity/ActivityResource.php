<?php

namespace App\Http\Resources\v1\Activity;

use Illuminate\Http\Resources\Json\JsonResource;

class ActivityResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'title' => $this->title,
            'date' => $this->date,
            'description' => $this->description,
        ];
    }
}
