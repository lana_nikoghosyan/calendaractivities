<?php

namespace App\Models\RoleAndPermission;

use App\Models\RoleAndPermission\Enums\RoleType;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    const ROLE_SUPER_ADMIN = 'super-admin';
    const ROLE_USER = 'user';

    /**
     * @var string[]
     */
    protected $fillable = ['name', 'guard_name'];

    public static function getRolesFormatted(): string
    {
        return implode('|', RoleType::ALL);
    }
}
