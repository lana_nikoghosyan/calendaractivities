<?php

namespace App\Models\Activity;

use App\Models\Base\Search;
use Illuminate\Database\Eloquent\Builder;

class ActivitySearch extends Search
{
    protected array $orderables = [
        'id',
        'title'
    ];

    protected function query(): Builder
    {
        $filters = $this->filters;

        return Activity::select([
            'id',
            'title',
            'date'
        ])
            ->when(!empty($filters['search']), function ($query) use ($filters) {
                $query->likeOr(['id', 'title'], $filters);
            })
            ->when(!empty($filters['id']), function ($query) use ($filters) {
                $query->where('id', $filters['id']);
            })
            ->when(!empty($filters['title']), function ($query) use ($filters) {
                $query->like('title', $filters['title']);
             });
    }

    public function totalCount(): int
    {
        return Activity::count();
    }
}
