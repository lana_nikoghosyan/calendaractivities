<?php

namespace App\Models\Activity;

use App\Models\Base\BaseModel;
use App\Models\Base\Traits\HasFileData;
use App\Models\File\File;
use App\Models\User\User;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Activity extends BaseModel
{

    const MAX_ACTIVITIES_PER_DAY = 4;
    use HasFileData;
    /**
     * @var string[]
     */
    protected $fillable = [
        'title',
        'description',
        'date'
    ];

    public function image(): MorphOne
    {
        return $this->morphOne(File::class, 'fileable')->where('field_name', 'image');
    }

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class)->withTimestamps();
    }
}
