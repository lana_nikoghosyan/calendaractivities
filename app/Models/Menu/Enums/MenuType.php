<?php

namespace App\Models\Menu\Enums;

final class MenuType
{
    final public const ADMIN = 'admin';
    final public const PROFILE = 'profile';
}
