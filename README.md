# CalendarActivities



## Getting started

In order to run the project please follow the steps

```
composer install,
create .env file and copy/paste the .env.example content into the one created,
npm i && npm run dev
create database 'calendar'
php artisan migrate
php artisan db:seed
```

## And you are good to go !!!
