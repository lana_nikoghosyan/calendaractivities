const options = {
  pathOptions: {
    searchPath: route('dashboard.activities.getListData'),
    deletePath: route('dashboard.activities.destroy', ':id'),
    editPath: route('dashboard.activities.edit', ':id'),
    showPath: route('dashboard.activities.show', ':id'),
  },

  relations: {},

  actions: {
      show: false,
  },
};
// eslint-disable-next-line no-new,no-undef
new DataTable(options);
